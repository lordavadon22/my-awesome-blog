from django.shortcuts import render

from .models import Event


def home(request):
    events = Event.objects.all()
    context = {
        'events': events
    }
    return render(request, 'events/home.html', context)


def about_me(request):
    return render(request, 'events/about.html')
